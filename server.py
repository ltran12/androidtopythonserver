import socket
import serial


def  connectSerial():
    p = 0
    for i in range(2):
         try:
             name = "/dev/ttyACM" + str(i)

             device = serial.Serial(name,timeout=1) 
             print 'name ' + name
             p  = 1
             return 1,device
         except serial.SerialException:
             p = 0
             pass
         if p == 0:
             print 'Device not found'

    return (0, '')

def main():

    HOST = ''                 # Symbolic name meaning the local host
    PORT = 50007              # Arbitrary non-privileged port
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
    s.bind((HOST, PORT))
    
    while 1:
        s.listen(1)
        
        while 1:
            conn, addr = s.accept()
            print 'Connected by', addr
        
            data = conn.recv(1024)
            print data

            p,dev = connectSerial()

            if p==1:
                dev.write(data)
#                st = dev.readline()
#                conn.send(st)
#                print st

            else:
                pass


            if not data: break
#            conn.send(data)
                    
    device.close()
    conn.close()
    print 'Connection closed'

if __name__ == "__main__":
    connectSerial()
    main()
