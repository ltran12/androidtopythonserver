#!/usr/bin/python

import sys, string, os
import RPi.GPIO as GPIO

device1 = 24
device2 = 23


def main():
    print sys.argv
    data = sys.argv[1]
    if data == "4341111":
        GPIO.output(device1, GPIO.HIGH)
        print "device 1 ON"
    elif data == "4341110":
        GPIO.output(device1, GPIO.LOW)
        print "device 1 OFF"
    elif data == "4341121":
        GPIO.output(device2, GPIO.HIGH)
        print "device 2 ON"
    elif data == "4341120":
        GPIO.output(device2, GPIO.LOW)
        print "device 2 OFF"



if __name__ == "__main__":
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(23, GPIO.OUT)
    GPIO.setup(24, GPIO.OUT)
    
    main()
