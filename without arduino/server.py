import socket
import RPi.GPIO as GPIO

device1 = 23
device2 = 24
device3 = 27
device4 = 22

def main():

    HOST = ''                 # Symbolic name meaning the local host
    PORT = 50007              # Arbitrary non-privileged port
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
    s.bind((HOST, PORT))
    
    while 1:
        s.listen(1)
        
        while 1:
            conn, addr = s.accept()
            print 'Connected by', addr
        
            data = conn.recv(1024)
            if data == "4341110":
                GPIO.output(device1, GPIO.HIGH)
                print "device 1 ON"
            elif data == "4341111":
                GPIO.output(device1, GPIO.LOW)
                print "device 1 OFF"
            elif data == "4341120":
                GPIO.output(device2, GPIO.HIGH)
                print "device 2 ON"
            elif data == "4341121":
                GPIO.output(device2, GPIO.LOW)
                print "device 2 OFF"
            elif data == "4341130":
                GPIO.output(device3, GPIO.HIGH)
                print "device 3 ON"
            elif data == "4341131":
                GPIO.output(device3, GPIO.LOW)
                print "device 3 OFF"
            elif data == "4341140":
                GPIO.output(device4, GPIO.HIGH)
                print "device 2 ON"
            elif data == "4341141":
                GPIO.output(device4, GPIO.LOW)
                print "device 2 OFF"


            if not data: break
#            conn.send(data)
                    
    device.close()
    conn.close()
    print 'Connection closed'

if __name__ == "__main__":
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(22, GPIO.OUT)
    GPIO.setup(27, GPIO.OUT)
    GPIO.setup(23, GPIO.OUT)
    GPIO.setup(24, GPIO.OUT)
    
    main()
